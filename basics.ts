let age: number;

age = 20;

let userName: string;

userName = "PabauUser";

let isGood: boolean;

isGood = true;


let hobbies: string[]; //array of strings

hobbies = ["Basketball", "Gaming", "Something"];


let person: {
    name: string;
    age: number;
};

person = {
    name: "User",
    age: 30
};


let people: {
    name: string;
    age: number;
}[];  // array of objects of this type ([])